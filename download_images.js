var rp = require('request-promise');
var fs = require('fs');
var _ = require('lodash-node');
var url = require("url");
var process = require('process');

var path = "/tmp";
var stateFile = "/var/oac_state";

/* -- Example config file --

var config = {};

config.deviceId = "xxx";

config.mailFrom = 'Daycare <me@gmail.com>';
config.mailTo = 'user@mail.com, user2@mail.com';
config.mailUsername = "me@gmail.com";
config.mailPassword = 'my_password';

config.uid = "nobody";
config.gid = "nogroup";

module.exports = config;
*/

var config = require('/etc/daycare2.config');

var OAC = {
    API: {
        GetUnseen: function (deviceId) {
            return OAC.API.GetFeed(deviceId).promise().get("items").then(function (feed) {
                return OAC.State.Unseen(feed);
            });
        },
        GetFeed: function (deviceId) {
            return rp.post("https://app.oacconnect.com/api/json/reply/Feed", {
                json: true,
                headers: {
                    'Accept': 'application/json',
                    'User-Agent': 'ServiceStack .NET Client 4.021',
                    'Expect': '100-continue'
                },
                body: {
                    "DeviceId": deviceId,
                    "Take": 20,
                    "Skip": 0,
                    "Filter": {
                        "ShowAboutKids": true,
                        "ShowMustKnow": true,
                        "ShowCentreInfo": true,
                        "Version": 100
                    },
                    "Width": 300,
                    "Height": 300,
                    "MinFeedId": 0,
                    "MaxFeedId": 0,
                    "AppVersion": 2.4,
                    "Version": 100
                }
            });
        },
        Download: function (_url, filePath) {
            rp.get(_url).pipe(fs.createWriteStream(filePath))
        }
    },
    State: {
        Unseen: function (items) {
            var lastSeenId = OAC.State.LastSeen();
            var unseenItems = _.takeWhile(items, function (item) {
                return item.id > lastSeenId;
            });

            if (unseenItems.length > 0) {
                OAC.State.UpdateLastSeen(unseenItems[0].id);
            }

            return unseenItems;
        },
        LastSeen: function () {
            if (fs.existsSync(stateFile)) {
                return fs.readFileSync(stateFile);
            }
            else {
                return 0;
            }
        },
        UpdateLastSeen: function (id) {
            fs.writeFileSync(stateFile, id);
        }
    }
};

var dump_file = function (item) {
    var newDir = path + "/" + item.id;

    if (!fs.existsSync(newDir)) {
        fs.mkdirSync(newDir);
    }

    fs.writeFileSync(newDir + "/description.html", item.text);

    _.forEach(item.content, function (content) {
        var filename = url.parse(content.filePath).pathname.split("/").pop();
        download(content.filePath, newDir + "/" + filename + ".png")
    });
};

var strip_size = function (imageUrl) {
    var u = url.parse(imageUrl, true);
    delete u.search;
    delete u.query.width;
    delete u.query.height;
    return url.format(u);
};

var send_email = function (item) {
    var nodemailer = require('nodemailer');

// create reusable transporter object using SMTP transport
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: config.mailUsername,
            pass: config.mailPassword
        }
    });

    var attachments = _.map(item.content, function (content) {
        var imageUrl = content.filePath;
        return {
            filename: url.parse(imageUrl).pathname.split("/").pop() + ".png",
            path: strip_size(imageUrl)
        };
    });

// setup e-mail data with unicode symbols
    var mailOptions = {
        from: config.mailFrom, // sender address
        to: config.mailTo,//, baz@blurdybloop.com', // list of receivers
        subject: item.title, // Subject line
        //text: 'Hello world ✔', // plaintext body
        html: item.text, // html body
        attachments: attachments
    };

// send mail with defined transport object
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        }
    });
};

try {
    if (config.gid) {
        process.setgid(config.gid);
    }
    if (config.uid) {
        process.setuid(config.uid);
    }
} catch (err) {
    console.log('Refusing to keep the process alive as root.');
    console.log(err);
    process.exit(1);
}

OAC.API.GetUnseen(config.deviceId).then(function (items) {
    _.forEach(items, function (item) {
        send_email(item);
    });
});